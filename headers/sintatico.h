#include <unordered_map>
#include <string>
#include "token.h"
#include "lexer.h"
#include "tabSimb.h"
using std::unordered_map;
using std::string;

class Sintatico
{
private:
	Token atual;
    Lexer lexer=Lexer();
	TabSimb* tabelaAtual;
	int escopNum;

public:
	string codigo;
	void analise(string codigo);
	void numOrVar(Token* sinalAnterio=NULL);
	void findExp(Token* sinalAnterio=NULL);
};