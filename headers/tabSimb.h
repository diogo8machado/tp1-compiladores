
#include <unordered_map>
#include "token.h"
#include <string>
using std::unordered_map;
using std::string;


class TabSimb
{
public:
	unordered_map<string, string> tab;
	TabSimb *pai;
	unordered_map<string,string>::const_iterator achar(TabSimb* tabelaAtual, Token atual);
};

