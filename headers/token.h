#include <unordered_map>
#include <string>
using std::unordered_map;
using std::string;
#if !defined(__TOKEN_H__) 
#define __TOKEN_H__

class Token
{
public:
	string type;
	string name;
	double value;
	int linha;
	Token();
	Token(string type,int linha);
	Token(string type, double value,int linha);
	Token(string type, string name,int linha);
};

#endif
