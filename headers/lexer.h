#include <unordered_map>
#include <string>
#include "token.h"
#include "error.h"
using std::unordered_map;
using std::string;
// analisador léxico
class Lexer
{
private:
	int  line = 1;
	char peek = ' ';
	int count=-1;

public:
	Lexer();
	Token Scan(string x);
	void Start();
};
