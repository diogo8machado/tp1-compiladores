#include "./headers/sintatico.h"
#include <iostream>
#include <fstream>
#include <string>
using namespace std;


int main(int argc, char **argv)
{
  //verifica se tem --help
  if((argv[1]=="--help")||(argv[2]=="--help")){
    cout<<"Bem vindo a ajuda do tradutor\nPrimeiraente o codigo ler um arquivo \".exp\", em seguida nesse arqivo deve ter um codigo que iniciar com expression, um \"{\" e no final um \"}\".";
     cout<<"\nDentro do par de \"{}\" é permitido fazer 3 coisas:\n";
     cout<<"-Declara e chamar variaveis: para declara basta usar int ou float mais o nome da variavel, para chamar/utilizar uma variavel precisa primeiramente declara ele, depois de declarada basta chamar pelo nome dela em alguma expressão.\n";
     cout<<"Criar escopos para novos contexto: é possivel criar um novo escopo apenas adcionando umpar e \"{}\", com isso é possivel redeclarar variaveis com omesmo nome sem alterar a variavel do escopo anterior.\n"
     cout<<"Criar um expressão: uma expression é uma equação matematica, exemplo \"2+(2*3+2)\", podendo no lugar dos numeros colocar variaveis.A expressao tem precedentes, que significa dizer que uma parte da equação deve ser resolvido primeiro que a outra, usando o exemplo 2+(2*3+2), precebe-se que  prmeiro temos que resolver o parêntese ante de adionar ao dois, em sequida temos que resolver primeiro a multiplicação 2*3 para entao adcionar o 2. Percebemos entao que temos 3 niveis de precedente, (), * ou /, e + ou -, sendo () com proridade maxima,seguido de * ou / e por ultimo + ou -\n";
     cout<<"Regras:\n-toda variavel precisa ser declarada\n-todo declaração de varivavel ou expressão precisa terminar com \";\"\n-todas as chaves de escopo precisam de seus respectivos par\n-Em uma expressão todos os parêntese precisam de seus respectivos pares\n "
  }else{//se não tiver continua o programar
    string codigo;
    cout<<argv[1];
    string filetype;
    for(int i=(argv.size()-5);i<(argv.size()-1);i++){//pega a extenção para verificar se é .exp
      filetype+=argv[i];
    }
    if(filetype==".exp"){//verifica se é .exp
      ifstream myfile (argv[1]); 
      if (myfile.is_open())
      {
        while (! myfile.eof() )
        {
          codigo+= myfile.get(); //ler o arquivo e armazena na string codigo;
        }
      cout << codigo << endl;
        myfile.close();
      }

      else cout << "Não foi possivel abrir o arquivo"; 
    try
            {
                Sintatico sintatico;
      sintatico.analise(codigo);//inicia o processo
      cout << endl;
            }
            catch (Error error)
            {
                    cout << "^\n";
                    cout << error.error<<" |Linha:"<<error.line;
            }
            cout << endl;
    }
  }
}
