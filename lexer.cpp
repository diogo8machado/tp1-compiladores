#include "./headers/lexer.h"
#include <iostream>
#include <sstream>
#include <string>
#include <math.h>
using std::cin;
using std::cout;
using std::stringstream;

// construtor
Lexer::Lexer()
{
}

// retorna tokens para o analisador sintático
Token Lexer::Scan(string x)//analizador lexo
{
	// salta espaços em branco, tabulações e novas linhas
	while ((isspace(peek)) || (peek == '\n'))
	{
		if (peek == '\n')
			line += 1; //adciona no contador
		peek = x[++count]; //passa pro proximo caracteres
	}
	//Ignora comentarios //
	if ((peek == '/') && (x[count + 1] == '/'))
	{
		while (peek != '\n')
		{
			peek = x[++count];
		}
		peek = x[++count];
		line += 1;
		return Scan(x);
	}
	//Ignora comentarios /* */
	if ((peek == '/') && (x[count + 1] == '*'))
	{
		bool sair = false;
		while (sair == false)
		{
			if (peek == '\n')
			{
				line++;
			}
			peek = x[++count];
			if ((peek == '*') && (x[count + 1] == '/'))
			{
				sair = true;
			}
		}
		count++;
		peek = x[++count];

		return Scan(x);
	}
	//cria token para ;
	if(peek==';'){
		Token token=Token(";",line);
		peek = x[++count];
		return token;
	}
	//cria token para (
	if(peek=='('){
		Token token=Token("(",line);
		peek = x[++count];
		return token;
	}
	//cria token para )
	if(peek==')'){
		Token token=Token(")",line);
		peek = x[++count];
		return token;
	}
	//cria token para {
	if(peek=='{'){
		Token token=Token("{",line);
		peek = x[++count];
		return token;
	}
	//cria token para }
	if(peek=='}'){
		Token token=Token("}",line);
		peek = x[++count];
		return token;
	}
	//cria token para + - * / do tipo sinal
	if((peek=='+')||(peek=='-')||(peek=='*')||(peek=='/')){
		Token token=Token("sinal",peek,line);
		peek = x[++count];
		return token;
	}
	// retorna números
	if (isdigit(peek))
	{
		double v = 0;
		bool ponto = false;
		int casaDecimal = -1;
		do
		{
			if (ponto == false)
			{
				// converte 'n' para o dígito numérico n
				int n = peek - '0';
				v = 10 * v + n;
				peek = x[++count];
				if (peek == '.')
				{
					ponto = true;
					peek = x[++count];
				}
			}
			else
			{ //depois de um ponto, calculo para ponto flutuante
				int n = peek - '0';
				v = v + (n * (pow(10, casaDecimal)));
				peek = x[++count];
				casaDecimal--;
			}
		} while (isdigit(peek));

		Token token=Token("num",v,line);
		return token;
	}
	// retorna palavras-chave e identificadores
	if (isalpha(peek))
	{
		stringstream ss;

		do
		{
			ss << peek;
			peek = x[++count];
		} while ((isalpha(peek)||isdigit(peek)));

		string s = ss.str();
		if((s=="int")||(s=="float")||(s=="expression")){//palavras resevadas
			Token token=Token(s,s,line);
		return token;
		}else{
			Token token=Token("ID",s,line);//IDs
			return token;
		}
		
	}else{
		throw Error{"Erro: Caracter nao esperado",line};//erro de carcteres que nao se encaixaram nas outras opções
	}
}

void Lexer::Start()
{
}
