
#include <iostream>
#include <sstream>
#include <string>
#include <math.h>
#include "./headers/token.h"
using std::cin;
using std::cout;
using std::stringstream;

// construtor
Token::Token()
{
    this->type="sair";
}
Token::Token(string type,int linha)
{
    this->type=type;
    this->linha=linha;
}
Token::Token(string type, double value,int linha)
{
    this->type=type;
    this->value=value;
    this->linha=linha;
}
Token::Token(string type, string name,int linha)
{
    this->type=type;
    this->name=name;
    this->linha=linha;
}