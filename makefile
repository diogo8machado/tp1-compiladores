# compilador 
CPP=g++
ARGS=-c -g -std=c++17

# dependências
all: expressions

expressions: expressions.o  lexer.o sintatico.o tabSimb.o  token.o
	$(CPP) expressions.o   lexer.o sintatico.o tabSimb.o  token.o -o expressions

expressions.o: expressions.cpp lexer.h 
	$(CPP) $(ARGS) expressions.cpp



sintatico.o: sintatico.cpp sintatico.h
	$(CPP) $(ARGS) sintatico.cpp

lexer.o: lexer.cpp lexer.h
	$(CPP) $(ARGS) lexer.cpp


tabSimb.o: tabSimb.cpp tabSimb.h
	$(CPP) $(ARGS) tabSimb.cpp
token.o: token.cpp token.h
	$(CPP) $(ARGS) token.cpp


clean:
	rm expressions expressions.o   lexer.o sintatico.o tabSimb.o token.o
