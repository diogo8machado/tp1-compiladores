#include "./headers/sintatico.h"
#include <iostream>
#include <sstream>
#include <string>
#include <math.h>

using std::cin;
using std::cout;
using std::stringstream;

void Sintatico::analise(string teste)//analisador sintatico
{

    this->codigo = teste;
    tabelaAtual = new TabSimb();//tabelroot sendo passada para atual
    tabelaAtual->pai = NULL;//tabela inicial(raiz), pai é null
    atual = lexer.Scan(codigo);//pega o primeiro token
    //
    if (atual.type == "expression")//verifica se é expression
    {
        atual = lexer.Scan(codigo);//pega segundo

        if (atual.type == "{")//verifica se é {
        {
            escopNum = 1;//aki guardo dizendo que o primeiro escopo foi aberto
            while (escopNum != 0)//continua ate acha o par do primeiro escopo
            {
                atual = lexer.Scan(codigo);
                if ((atual.type == "num") || (atual.type == "ID") || atual.type == "(")//verifica se pode ser um expressao
                {
                    findExp();//vai analisar e imprimir uma expressao posfixada 
                    cout << ";\n";
                }

                if (atual.type == "{")//verifica se abriu um novo escopo
                {
                    escopNum++;//adciona mais um no numero de escopo
                    TabSimb *filho = new TabSimb();
                    filho->pai = tabelaAtual;//passa a tabela atual para o pai da novo
                    tabelaAtual = filho;//nova fica no lugar do pai
                }
                if (atual.type == "}")//verifica se fechou o escopo
                {
                    escopNum--;
                    tabelaAtual = tabelaAtual->pai;//tabela atual aponta para a tabela pai
                }
                if ((atual.type == "int") || atual.type == "float")//verifica se é uma declacação de variavel
                {
                    Token type = atual;
                    atual = lexer.Scan(codigo);
                    if (atual.type == "ID") //verifica se o proximo é um id
                    {
                        Token variable = atual;
                        atual = lexer.Scan(codigo);
                        if (atual.type == ";")//e verifica se termina com ;
                        {
                            tabelaAtual->tab[variable.name] = type.name;//adiciona novo ID
                        }
                        else
                        {
                            throw Error{"Erro: Falta \";\"", atual.linha};
                        }
                    }
                    else
                    {
                        throw Error{"Erro: Não foi identificado um nome valido para variavel", atual.linha};
                    }
                }else{
                    throw Error{"Erro: Valor inesperado: " + atual.type, atual.linha};
                }
            }
        }
        else
        {
            throw Error{"Erro: Depois de \"expression\" usar \"{\"", atual.linha};
        }
    }
    else
    {
        throw Error{"Erro: a palavra expression não foi encontrado", atual.linha};
    }
}

void Sintatico::findExp(Token *sinalAnterio)
{
  
    if ((atual.type == "num") || (atual.type == "ID"))
    {
        if (atual.type == "ID")
        {
              auto pos = tabelaAtual->achar(tabelaAtual, atual);
            if (pos != tabelaAtual->tab.end())
            {
                
                cout << "(" << pos->first << ":" << pos->second << ")";
            }
            else
            {
                throw Error{"Erro: Variavel não declarada " + atual.name, atual.linha};
            }
        }
        else
        {
            cout << "(" << atual.value << ")";
        }

        Token sinal = lexer.Scan(codigo);

        if (sinal.type == "sinal")
        {
            if (((char)sinal.value == '-') || ((char)sinal.value == '+'))
            {
                if (sinalAnterio != NULL)
                {
                    cout << (char)sinalAnterio->value;
                }
                atual = lexer.Scan(codigo);
                findExp(&sinal);
                return;
            }
            if (((char)sinal.value == '/') || ((char)sinal.value == '*'))
            {
                if (sinalAnterio != NULL)
                {
                    if (((char)sinalAnterio->value == '/') || ((char)sinalAnterio->value == '*'))
                    {
                        cout << (char)sinalAnterio->value;
                        atual = lexer.Scan(codigo);
                        findExp(&sinal);
                        return;
                    }
                    else
                    {
                        atual = lexer.Scan(codigo);
                        findExp(&sinal);
                        cout << (char)sinalAnterio->value;
                        return;
                    }
                }
                else
                {
                    atual = lexer.Scan(codigo);
                    findExp(&sinal);
                    if (sinalAnterio != NULL)
                    {
                        cout << (char)sinalAnterio->value;
                    }
                    return;
                }
            }
        }
        if (sinal.type == ")")
        {
            atual = sinal;
            if (sinalAnterio != NULL)
            {
                cout << (char)sinalAnterio->value;
            }
            return;
        }
        if (sinal.type == ";")
        {
            if (sinalAnterio != NULL)
            {
                cout << (char)sinalAnterio->value;
            }

            return;
        }
        else
        {
            throw Error{"Erro: Falta um operador(+,-,*,/),ou \")\" ou \";\"", sinal.linha};
        }
    }
    if (atual.type == "(")
    {
        cout << "(";
        atual = lexer.Scan(codigo);
        findExp();
        Token sinal = lexer.Scan(codigo);
        if (atual.type == ")")
        {
            cout << ")";
            if (((char)sinal.value == '-') || ((char)sinal.value == '+'))
            {
                if (sinalAnterio != NULL)
                {
                    cout << (char)sinalAnterio->value;
                }
                atual = lexer.Scan(codigo);
                findExp(&sinal);
                return;
            }
            if (((char)sinal.value == '/') || ((char)sinal.value == '*'))
            {
                if (sinalAnterio != NULL)
                {
                    if (((char)sinalAnterio->value == '/') || ((char)sinalAnterio->value == '*'))
                    {

                        cout << (char)sinalAnterio->value;
                        atual = lexer.Scan(codigo);
                        findExp(&sinal);
                        return;
                    }
                    else
                    {
                        atual = lexer.Scan(codigo);
                        findExp(&sinal);
                        cout << (char)sinalAnterio->value;
                        return;
                    }
                }
                else
                {
                    if (sinalAnterio != NULL)
                    {
                        cout << (char)sinalAnterio->value;
                    }
                    atual = lexer.Scan(codigo);
                    findExp(&sinal);
                    return;
                }
            }
            if (sinal.type == ";")
            {
                if (sinalAnterio != NULL)
                {
                    cout << (char)sinalAnterio->value;
                }

                return;
            }
            else
            {
                throw Error{"Erro: Falta um operador(+,-,*,/),ou \")\" ou \";\"", sinal.linha};
            }
        }
    }
    else
    {
        throw Error{"Erro: Falta um numero ,ou variavel ou \"(\"", atual.linha};
    }
}