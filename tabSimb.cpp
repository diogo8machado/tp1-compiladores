#include "./headers/tabSimb.h"
#include <iostream>
#include <math.h>
using std::cin;
using std::cout;
using std::stringstream;
using std::unordered_map;

unordered_map<string,string>::const_iterator TabSimb::achar(TabSimb *tabelaAtual, Token atual)
{
    TabSimb *paser = tabelaAtual;
    unordered_map<string,string>::const_iterator pos;;
    while (paser != NULL)
    {
       pos = paser->tab.find(atual.name);
        if (pos != paser->tab.end())
        {
            paser = NULL;
        }
        else
        {
            paser = paser->pai;
        }
    }
    return pos;
}